//This is the website of the API that I used>> https://app.exchangerate-api.com/

"use strict";
// Select Elements:

const currencyEl_one = document.querySelector("#currency-one");
const currencyEl_two = document.querySelector("#currency-two");
const amountEl_one = document.querySelector("#amount-one");
const amountEl_two = document.querySelector("#amount-two");

const rateEl = document.querySelector("#rate");
const swap = document.querySelector("#swap");

const calculate = function () {
  const currency_one = currencyEl_one.value;
  const currency_two = currencyEl_two.value;
  rateEl.innerText = "Loading...";
  fetch(
    ` https://v6.exchangerate-api.com/v6/da7f22aa1e85e248b8503e17/latest/${currency_one}`
  ).then((res) =>
    res
      .json()
      .then((data) => {
        //   console.log(data);
        const rate = data.conversion_rates[currency_two];
        // console.log(rate);
        rateEl.innerText = `1 ${currency_one}  =  ${rate.toFixed(
          2
        )}${currency_two}`;
        amountEl_two.value = (amountEl_one.value * rate).toFixed(2);
      })
      .catch((err) => alert("Unable to fetch data from the API."))
  );
};

calculate();

//EventListeners:

currencyEl_one.addEventListener("change", calculate);
currencyEl_two.addEventListener("change", calculate);
amountEl_one.addEventListener("input", calculate);
amountEl_two.addEventListener("input", calculate);
swap.addEventListener("click", () => {
  const temp = currencyEl_one.value;
  currencyEl_one.value = currencyEl_two.value;
  currencyEl_two.value = temp;
  calculate();
});
